<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    return response(file_get_contents(base_path('test.pdf')))->header('Content-Type', 'application/pdf');
});

Route::get('/stream', function () {
    return response()->file(base_path('test.pdf'));
});

Route::get('/stream_with_content_length', function () {
    $path = base_path('test.pdf');

    return response()
        ->file($path, [
            'Content-Length' => filesize($path)
        ]);
});

Route::get('/stream_download', function () {
    return response()->streamDownload(function () {
        echo file_get_contents(base_path('test.pdf'));
    }, 'test.pdf', ['Content-Type' => 'application/pdf'], 'inline');
});
